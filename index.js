'use strict'
require('module-alias/register')
require('babel-polyfill')
require('babel-core/register')

global.config = require('./config')

// @TODO import sever as object and start manually from here.
// that way we can write extensions on a per-project-basis
require('../../../app/server')
