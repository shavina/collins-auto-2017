const elixir = require('laravel-elixir')

elixir(mix => {
	// Your Theme
	mix.sass('./private/styles/master.scss', './public/css/master.css')
	mix.rollup('./private/js/master.js', './public/js/master.js')

	// Vendor Dependencies
	// Build vendor scripts together to optimise http requests
	mix.rollup('./private/js/vendor.js', './public/js/vendor.js')
})
