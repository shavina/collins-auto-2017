(function (exports) {
'use strict';

// console.log('master.js')
$(document).ready(function(){
    $(".hamburger").click(function(){
        $(".mobile-menu").fadeToggle("slow");
    });


    $(function() {
      $(window).scroll(function() {
          if ($(document).scrollTop() >= 20) {
            $('.main-nav').addClass( "float-nav" );
          }
          else {
          if($('.main-nav').hasClass("float-nav") == true) {
            $('.main-nav').removeClass("float-nav");
          }
            }
      }).triggerHandler("scroll");
    });


});

}((this.LaravelElixirBundle = this.LaravelElixirBundle || {})));
//# sourceMappingURL=master.js.map
